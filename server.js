import log from 'loglevel'
import { iso, now } from './utils.js'
const errorlog = e => log.error(`[${now()}]: Error: ${e.stack}.`)

log.setLevel(process.env.loglevel || "SILENT")
log.info(`Log level ${['TRACE', 'DEBUG', 'INFO', 'WARN', 'ERROR', 'SILENT'][log.getLevel()]}.`)

async function job() {
  const start = new Date()
  try {
  } catch (e) {
    errorlog(e)
  }
}

import schedule from 'node-schedule'

let running = false
// every 15 minutes except Sunday
const job = schedule.scheduleJob('15 * * * 1-6',  async function() {
  try {
    if (!running) {
      log.info(`[${now()}]: Job started.`)
      running = true
      await job()
      running = false
    } else {
      log.info(`[${now()}]: Previous job has not finished. Skipped.`)
    }
    log.info(`[${now()}]: Next job is scheduled at ${iso(job.nextInvocation())}.`)
  } catch (e) {
    errorlog(e)
  }
})

log.info(`[${now()}]: Next job is scheduled at ${iso(job.nextInvocation())}.`)

import express from 'express'
const app = express()
const port = process.env.port || 8080

app.get('/hi', (req, res) => {
  res.send('Hello, world!')
})
app.listen(port, () => {
  log.info(`[${now()}]: Listening on port ${port}.`)
})
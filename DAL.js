import log from 'loglevel'
import tedious from 'tedious'
const { Connection, Request, ISOLATION_LEVEL } = tedious

/*
const lcfirst = string => string.charAt(0).toLowerCase() + string.slice(1)
const ucfirst = string => string.charAt(0).toUpperCase() + string.slice(1)
*/
const STATE = { INIT: 0, DIAL: 1, IDLE: 2, BUSY: 3, FAIL: 4 }

class DAL {
  constructor(dbConfig, options) {
    try {
      this.tablePrefix = dbConfig.tablePrefix
      this.config = {
        server: dbConfig.server,
        authentication: {
          type: 'default',
          options: {
            userName: dbConfig.username,
            password: dbConfig.password
          }
        },
        options: {
          encrypt: false,
          rowCollectionOnRequestCompletion: true,
          useColumnNames: true,
          /*columnNameReplacer: (k=>lcfirst(k.replace(/[)(_]/g,"")
            .split(/[\s\-]+/).reduce((a, c)=>a+=ucfirst(c)))),
          */
          database: dbConfig.name,
          isolationLevel: ISOLATION_LEVEL.READ_COMMITTED,
          connectionIsolationLevel: ISOLATION_LEVEL.READ_COMMITTED
        }
      }
      if (options) for(let k in options) this.config.options[k]=options[k]
      this.connection = null
      this.state = STATE.INIT
      this.jobQ = []
    } catch (e) {
      const msg = 'Fail to initialize database.  Please check config file.'
      throw Error(msg)
    }
  }

  dial() {
    this.state = STATE.DIAL
    this.connection = new Connection(this.config)
    this.connection.connect() // this is required since version 9
    this.connection.on('connect', err => {
      if (err) this.init()
      else this.idle()
    })
    this.connection.on('error', () => { this.fail() })
    this.connection.on('end', () => {
      if (this.state === STATE.FAIL) this.dial()
      else this.init()
    })
    //this.connection.on('debug', text => log.debug(`DAL: ${text}`))
  }
  
  idle() {
    if (this.jobQ.length > 0) {
      this.busy()
      const job = this.jobQ.shift()
      const {action, stmt, parameters, resolve, reject} = job

      const E = err => {
        if (err) reject(err); else resolve()
        this.idle()
      }
      switch (action) {
        case 'BEGIN':  this.connection.beginTransaction(E); return
        case 'COMMIT': this.connection.commitTransaction(E); return
        case 'ROLL':   this.connection.rollbackTransaction(E); return
      }

      const request = new Request(stmt, function(err, _, rows) {
        if (err) {
          reject(err)
        } else {
          let records = rows.map(r=>Object.keys(r).reduce((o, k)=>{
            let v = r[k].value
            o[k] = (typeof v === 'number') ? Math.round(v*1000000)/1000000 : v
            return o
          }, {}))
          resolve(records)
        }
      })
      request.on('requestCompleted', ()=>{ this.idle() })
      if (parameters && parameters instanceof Array) {
        parameters.forEach(p=>request.addParameter(p.name, p.type, p.value))
      }
      if (action === 'SQL') this.connection.execSql(request)
      else if (action === 'SP') this.connection.callProcedure(request)
    } else this.state = STATE.IDLE  // jobQ empty
  }
  
  busy() {
    this.state = STATE.BUSY
  }
  
  fail(err) {
    log.error(`DAL enters fail state: Error: ${err}.`)
    this.state = STATE.FAIL
  }

  init() {
    while (this.jobQ.length > 0) {
      this.jobQ.shift().reject(this.state===STATE.DIAL
        ? 'Cannot establish a database connection.'
        : 'Database connection lost.'
      )
    }
    this.state = STATE.INIT
  }

  addJob(action, stmt, parameters) {
    if (stmt) stmt = stmt.replace(/%tablePrefix%/gi, this.tablePrefix)
    return new Promise((resolve, reject)=>{
      this.jobQ.push({action, stmt, parameters, resolve, reject})
      if (this.state===STATE.IDLE) this.idle()
      else if (this.state===STATE.INIT) this.dial()
    })
  }

  close() {
    this.init()
    this.connection.close()
  }
}

export default class Database {
  constructor(db, options) {
    this.database = new DAL(db, options)
  }

  execSql(sql, parameters) {
    return this.database.addJob('SQL', sql, parameters)
  }

  callProcedure(sp, parameters) {
    return this.database.addJob('SP', sp, parameters)
  }

  close() {
    this.database.close()
  }

  begin() {
    return this.database.addJob('BEGIN')
  }
  
  commit() {
    return this.database.addJob('COMMIT')
  }

  rollback() {
    return this.database.addJob('ROLL')
  }
}

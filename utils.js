import dateFormat from 'dateformat'
const iso = d => dateFormat(d, 'isoTime')
const now = _ => iso(new Date())

export {iso, now}
